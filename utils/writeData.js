const fs = require("fs");

function writeData(filename, data) {
  const content = JSON.stringify(data, null, 2);
  fs.writeFile(filename, content, (err) => {
    if (err) {
      console.log("Error occured while writing file");
      console.error(err);
    } else {
      // file written successfully
    }
  });
}

module.exports = writeData;
