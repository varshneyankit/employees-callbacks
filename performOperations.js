const fs = require("fs");

const data = require("./data.json");
const writeData = require("./utils/writeData");

const getEmployeesById = require("./problem1");
const groupDataBasedOnCompanies = require("./problem2");
const getDataOfSpecificCompany = require("./problem3");
const removeEntryBasedOnId = require("./problem4");
const sortDataBasedOnCompany = require("./problem5");
const swapCompanyByIds = require("./problem6");
const addBirthdayToEmployeesWithEvenId = require("./problem7");

function performOperations() {
  fs.mkdir("./output", { recursive: true }, (err) => {
    if (err) {
      console.log("Error occured while creating folder");
      console.error(err);
    } else {
      console.log("Folder created successfully");

      getEmployeesById([2, 13, 23], data, (err, result) => {
        if (err) {
          console.error(err);
        } else {
          writeData("./output/1-employees-by-id.json", result);

          groupDataBasedOnCompanies(data, (err, result) => {
            if (err) {
              console.error(err);
            } else {
              writeData("./output/2-groups-based-on-company.json", result);

              getDataOfSpecificCompany(
                "Powerpuff Brigade",
                data,
                (err, result) => {
                  if (err) {
                    console.error(err);
                  } else {
                    writeData(
                      "./output/3-data-of-specific-company.json",
                      result
                    );

                    removeEntryBasedOnId(2, data, (err, result) => {
                      if (err) {
                        console.error(err);
                      } else {
                        writeData("./output/4-remove-entry-by-id.json", result);

                        sortDataBasedOnCompany(data, (err, result) => {
                          if (err) {
                            console.error(err);
                          } else {
                            writeData(
                              "./output/5-sort-data-based-on-company.json",
                              result
                            );

                            swapCompanyByIds(92, 93, data, (err, result) => {
                              if (err) {
                                console.error(err);
                              } else {
                                writeData(
                                  "./output/6-swap-companies-by-id.json",
                                  result
                                );

                                addBirthdayToEmployeesWithEvenId(
                                  data,
                                  (err, result) => {
                                    if (err) {
                                      console.error(err);
                                    } else {
                                      writeData(
                                        "./output/7-add-birthday-to-even-id.json",
                                        result
                                      );
                                    }
                                  }
                                );
                              }
                            });
                          }
                        });
                      }
                    });
                  }
                }
              );
            }
          });
        }
      });
    }
  });
}

module.exports = performOperations;
