function getEmployeesById(givenIds, data, callback) {
  try {
    if (
      !Array.isArray(givenIds) ||
      typeof data != "object" ||
      typeof callback != "function"
    ) {
      throw new Error("Data is incorrect");
    } else {
      const employees = data.employees.filter((employeeData) => {
        const employeeId = employeeData.id;
        if (givenIds.includes(employeeId)) {
          return true;
        } else {
          return false;
        }
      });

      callback(null, employees);
    }
  } catch (error) {
    console.error(error);
  }
}

module.exports = getEmployeesById;
