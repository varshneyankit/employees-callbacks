const performOperations = require("../performOperations");

try {
  performOperations();
} catch (error) {
  console.error(error);
}
