function swapCompanyByIds(firstId, secondId, data, callback) {
  try {
    if (
      typeof firstId != "number" ||
      typeof secondId != "number" ||
      typeof data != "object" ||
      typeof callback != "function"
    ) {
      throw new Error("Data is incorrect");
    } else {
      let firstIndex = undefined;
      let secondIndex = undefined;

      data.employees.forEach((employeeData, index) => {
        if (employeeData.id == firstId) {
          firstIndex = index;
        }
        if (employeeData.id == secondId) {
          secondIndex = index;
        }
      });

      const tempData = data.employees[firstIndex];
      data.employees[firstIndex] = data.employees[secondIndex];
      data.employees[secondIndex] = tempData;

      callback(null, data);
    }
  } catch (error) {
    console.error(error);
  }
}

module.exports = swapCompanyByIds;
