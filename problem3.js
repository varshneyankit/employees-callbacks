function getDataOfSpecificCompany(companyName, data, callback) {
  try {
    if (
      typeof companyName != "string" ||
      typeof data != "object" ||
      typeof callback != "function"
    ) {
      throw new Error("Data is incorrect");
    } else {
      const companyEmployees = data.employees.reduce(
        (accumulator, employeeData) => {
          const employeeId = employeeData.id;
          const employeeName = employeeData.name;
          const currentCompanyName = employeeData.company;

          if (currentCompanyName == companyName) {
            accumulator.push({
              id: employeeId,
              name: employeeName,
            });
          }

          return accumulator;
        },
        []
      );

      const result = {
        [companyName]: companyEmployees,
      };

      callback(null, result);
    }
  } catch (error) {
    console.error(error);
  }
}

module.exports = getDataOfSpecificCompany;
