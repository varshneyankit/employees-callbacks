function removeEntryBasedOnId(givenId, data, callback) {
  try {
    if (
      typeof givenId != "number" ||
      typeof data != "object" ||
      typeof callback != "function"
    ) {
      throw new Error("Data is incorrect");
    } else {
      const result = data.employees.reduce((accumulator, employeeData) => {
        const employeeId = employeeData.id;
        if (employeeId != givenId) {
          accumulator.push(employeeData);
        }
        return accumulator;
      }, []);

      callback(null, result);
    }
  } catch (error) {
    console.error(error);
  }
}

module.exports = removeEntryBasedOnId;
