function groupDataBasedOnCompanies(data, callback) {
  try {
    if (typeof data != "object" || typeof callback != "function") {
      throw new Error("Data is incorrect");
    } else {
      const result = data.employees.reduce((accumulator, employee) => {
        const employeeId = employee.id;
        const employeeName = employee.name;
        const employeeCompany = employee.company;

        if (accumulator[employeeCompany] == undefined) {
          accumulator[employeeCompany] = [];
        }
        accumulator[employeeCompany].push({
          id: employeeId,
          name: employeeName,
        });

        return accumulator;
      }, {});

      callback(null, result);
    }
  } catch (error) {
    console.error(error);
  }
}

module.exports = groupDataBasedOnCompanies;
