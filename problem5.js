function sortDataBasedOnCompany(data, callback) {
  try {
    if (typeof data != "object" || typeof callback != "function") {
      throw new Error("Data is incorrect");
    } else {
      const givenData = JSON.parse(JSON.stringify(data));
      const result = givenData.employees.sort((employeeOne, employeeTwo) => {
        if (employeeOne.company > employeeTwo.company) {
          return 1;
        } else if (
          employeeOne.company == employeeTwo.company &&
          employeeOne.id > employeeTwo.id
        ) {
          return 1;
        } else {
          return -1;
        }
      });

      callback(null, result);
    }
  } catch (error) {
    console.error(error);
  }
}

module.exports = sortDataBasedOnCompany;
