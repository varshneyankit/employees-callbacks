function addBirthdayToEmployeesWithEvenId(data, callback) {
  try {
    if (typeof data != "object" || typeof callback != "function") {
      throw new Error("Data is incorrect");
    } else {
      data.employees.forEach((employeeData) => {
        const employeeId = employeeData.id;
        if (employeeId % 2 == 0) {
          const date = new Date();
          let day = date.getDate();
          let month = date.getMonth() + 1;
          let year = date.getFullYear();
          let currentDate = `${day}-${month}-${year}`;
          employeeData.birthday = currentDate;
        }
      });

      callback(null, data);
    }
  } catch (error) {
    console.error(error);
  }
}

module.exports = addBirthdayToEmployeesWithEvenId;
